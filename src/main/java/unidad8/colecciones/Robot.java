package unidad8.colecciones;

public class Robot {
	private String nombre;
	private int segundos;
	private int cuentaAtras;

	public Robot(String nombre, int segundos) {

		this.nombre = nombre;
		this.segundos = segundos;
	}

	public void asignarProducto(String producto, String hora) {
		System.out.println(nombre + " - " + producto + "[" + hora + "]");
		cuentaAtras = segundos;

	}

	public boolean procesarProducto() {
		if (cuentaAtras > 0)
			cuentaAtras--;
		return cuentaAtras == 0;
	}
}
