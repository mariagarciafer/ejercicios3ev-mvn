  package unidad8.colecciones;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Palabras {
	/*Uso un mapa , la clave es un número entero y el conjunto que no se repite */
	private Map<Integer, Set<String>> mapa;
	
	public Palabras() {
		mapa = new TreeMap<>();
	}
	/*Constructor que almacena las palabras que hay en un texto*/
	public Palabras(String texto) {
		this();
		addPalabras(texto);
	}
	/*Método para añadir palabras*/
	private void add(String palabra) {
		int longitud = palabra.length();
		Set<String> palabras = mapa.get(longitud);
		if (palabras == null)
			mapa.put(longitud, palabras = new TreeSet<>());
		palabras.add(palabra);
	}
	/*Método para añadir las palabras de una cadena de caractéres*/
	public void addPalabras(String texto) {
		Scanner s = new Scanner(texto);
		s.useDelimiter("\\P{L}+");
		while (s.hasNext())
			add(s.next());
		s.close();
	}
	
//	public void addPalabras(String texto) {
//		for(String palabra: texto.split("\\P{L}+"))
//			add(palabra);
//	}
	/*Método para comprobar que una palabra está añadida*/
	public boolean contiene(String palabra) {
		try {
			return mapa.get(palabra.length()).contains(palabra);
		} catch (NullPointerException e) {
			return false;/*la palabra no está en el mapa*/
		}
	}
	
//	public String [] get(int longitud) {
//		String [] a = new String[mapa.get(longitud).size()];
//		mapa.get(longitud).toArray(a);
//		return a;
//	}
	
	public Set<String> get(int longitud) {
		return Collections.unmodifiableSet(mapa.get(longitud));
	}
	
	public void eliminarTodas() {
		mapa.clear();
	}
}
