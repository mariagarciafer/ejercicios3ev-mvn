package unidad8.colecciones;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class ColeccionesTest {

	@Test
	public void testEliminarLasdeLongitudPar() {
		Set<String> set = new HashSet<>(Set.of("uno", "dos", "tres"));
		Colecciones.eliminarLasdeLongitudPar(set);
		assertEquals(set, Set.of("uno", "dos"));
	}

	@Test
	public void testContieneImpares() {
		fail("Not yet implemented");
	}

	@Test
	public void testValoresUnicos() {
		fail("Not yet implemented");
	}

}
